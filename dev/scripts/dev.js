(($) => {
    "use strict";

    // (function ($) {
    //     $.fn.rewAccordion = function() {
    //         let make = function () {
    //             const accordionLinks = $(this).find('.accordion_heading_open');
    //             const accordionContent = $(this).find('.accordion__panel');
    //
    //             for (let i = 0; i < accordionLinks.length; i++) {
    //                 $(accordionLinks[i]).click(function(e) {
    //                     e.preventDefault();
    //
    //                     const j = $(accordionLinks).index(this);
    //
    //                     if ($(this).hasClass('active')) {
    //
    //                         $(this).removeClass('active');
    //                         $(accordionContent[j]).slideUp();
    //
    //                     } else {
    //
    //                         $(accordionLinks).removeClass('active');
    //                         $(accordionContent).slideUp();
    //
    //                         $(this).addClass('active');
    //                         $(accordionContent[j]).slideDown();
    //                     }
    //                 });
    //             }
    //         };
    //         return this.each(make);
    //     };
    // })(jQuery);

    //Accordion
    $(".accordion").on("click", ".accordion_heading_open", function() {
        $(this).toggleClass("active").next().slideToggle();
        $(".accordion__panel").not($(this).next()).slideUp(300);
        $(this).siblings().removeClass("active");
    });
    //End Accordion

    (function ($) {
        $.fn.rewTabs = function() {
            let make = function () {
                const tabLinks = $(this).find('.tabs li a');
                const tabContent = $(this).find('.tabs-content');


                        for (let i = 0; i < tabLinks.length; i++) {
                            $(tabLinks[i]).click(function(e) {
                                e.preventDefault();

                                $(tabLinks).removeClass('active');
                                $(tabContent).removeClass('active');

                        let j = $(tabLinks).index(this);

                        $(this).addClass('active');
                        $(tabContent[j]).addClass('active');
                    });
                }
            };
            return this.each(make);
        };
    })(jQuery);


    const DOMs = {

        documentBodyHtml: $("html, body"),
        // documentBody: $("body"),

        menuTrigger: $("#mobile__icon"),

        // selectChosen: $(".chosen-select"),
        // selectSelect2: $(".select2-select"),
        // selectSelect2Container: $(".select-select2-container"),
        //
        sliderBlog: $("#blog__slider"),
        sliderPost: $(".post__slider"),
        // sliderArrowLeft : "",
        // sliderArrowRight : "",
        sliderProject : $(".portfolio__slider"),
        sliderCounter : $(".portfolio__slider__count__info"),
        //
        linkSmooth: $(".smooth"),
        // linkUp : $(".link-up"),
        //
        // accordion : $('#accordion'),
        tabs: $("#tabs"),
        // datepicker : $(".datepicker"),
        dropdown: $("#scroll_nav_dropdown"),
        dropdown_title: $('#dropdown_title'),
        dropdown_sticky: $(".scroll_nav_dropdown")

    };

    $(document).ready(() => {

        // ========== MENU ==========

        DOMs.menuTrigger.click((e) => {
            e.preventDefault();
            DOMs.documentBodyHtml.toggleClass("open");

        });



        // ========== SELECT ==========

        // ----- CHOSEN -----
        // DOMs.selectChosen.chosen({
        //     disable_search_threshold: 4,
        //     no_results_text: "Нічого не знайдено"
        // });

        // ----- SELECT2 -----
        // DOMs.selectSelect2.select2({
        //     placeholder: "Choose...",
        //     allowClear: true,
        //     dropdownParent: DOMs.selectSelect2Container,
        //     minimumResultsForSearch: Infinity
        // });

        // ========== SLIDER ==========
        //Slider Blog
        DOMs.sliderBlog.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            adaptiveHeight: true,
            responsive: [
                {
                    breakpoint: 600,
                    settings: {
                        fade: false,
                        adaptiveHeight: false,
                    }
                },
            ]
        });

        //Slider Post
        DOMs.sliderPost.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 500,
            cssEase: 'linear',
            pauseOnHover:true,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            // autoplay: true,
            // autoplaySpeed: 3500,
        });

        //Slider count Project
        DOMs.sliderProject.on("init reInit afterChange", (event, slick, currentSlide) => {
            let i = (currentSlide ? currentSlide : 0) + 1;
            DOMs.sliderCounter.text(i + '/' + slick.slideCount);
        });

        //Slider Project
        DOMs.sliderProject.slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            speed: 800,
            cssEase: 'linear',
            pauseOnHover:true,
            prevArrow: $('.prev'),
            nextArrow: $('.next'),
            // autoplay: true,
            // autoplaySpeed: 6000,
            fade: true
        });


        // ----- SLICK SLIDER -----
        // DOMs.sliderFull.slick({
        //     prevArrow: DOMs.sliderArrowLeft,
        //     nextArrow: DOMs.sliderArrowRight,
        //     dots: true
        // });

        // // ========== ACCORDION ==========
        // DOMs.accordion.rewAccordion();

        // // ========== TABS ==========
        // DOMs.tabs.rewTabs();

        // ========== DATEPICKER ==========
        // DOMs.datepicker.flatpickr();

        // ========== SCROLLSPY ==========

        // ----- SCROLLING CLASS CHANGE -----
        // $(window).scroll(() => {
        //     if ($(this).scrollTop() > 100) {
        //         DOMs.dropdown.addClass("scroll_nav_active");
        //     }
        //     else {
        //         DOMs.dropdown.removeClass("scroll_nav_active");
        //     }
        // });
        // declare variable
        // scroll END


        //Animation
    (function scrollReveal() {
        window.sr = ScrollReveal({
            reset: false,
            mobile: false
        });

        sr.reveal('.animate_left',{
            interval: 300,
            origin: 'left',
            distance: '50px',
            duration: 1500
        });

        sr.reveal('#tabs, #blog__slider, .portfolio__project', {
            origin: 'bottom',
            distance: '150px',
            duration: 1800,
            scale: 1
        });

        sr.reveal('.animate__caption', {
            origin: 'top',
            distance: '100px',
            duration: 1000,
            scale: 1
        });
        sr.reveal('.animate__text', {
            origin: 'bottom',
            distance: '100px',
            duration: 1000,
            scale: 1
        });
        sr.reveal('.animate__img', {
            interval: 300,
            origin: 'right',
            distance: '250px',
            duration: 1800,
            scale: 1
        });

        sr.reveal('.post__container', {
            interval: 400,
            origin: 'bottom',
            distance: '150px',
            duration: 1600,
            scale: 1
        });

        sr.reveal('.method__page__list, .service__page__content__block', {
            duration   : 600,
            distance   : '20px',
            easing     : 'ease-out',
            origin     : 'bottom',
            reset      : false,
            scale      : 1,
            viewFactor : 0,
            afterReveal  : revealChildren,
        }, 150);

        var revealChildren = sr.reveal('ol li', {
            duration   : 500,
            interval   : 300,
            scale      : 1,
            distance   : '20px',
            origin     : 'bottom',
            reset      : false,
            easing     : 'ease-out',
            viewFactor : 1,
        }, 75);


        })();
        //End Animation

        // ----- ANCHOR LINKS SCROLLING -----
        DOMs.linkSmooth.click(function (e) {
            e.preventDefault();

            var id = $(this).attr("href");
            var top = $(id).offset().top - 80;

            DOMs.documentBodyHtml.animate({
                scrollTop: top
            }, 500);

        });


        //Change active class on scroll
        var sections = $('.section');
        var nav = $('.scroll_nav_container');
        var nav_height = nav.outerHeight();

        $(window).on('scroll', function () {
            var cur_pos = $(this).scrollTop();

            sections.each(function() {
                var top = $(this).offset().top - nav_height,
                    bottom = top + $(this).outerHeight();

                if (cur_pos >= top && cur_pos <= bottom) {
                    nav.find('a').removeClass('active');
                    sections.removeClass('active');

                    $(this).addClass('active');
                    nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
                }
            });
        });
        //End Change active class on scroll


        //Dropdown hiring
        $(".hiring_item").click(function () {
            $('.scroll_nav').addClass("closed");
            $(".title p").text($(this).text());
            $('.scroll__overlay').removeClass("scroll__overlay__show");
            $(".hiring_item_hidden").removeClass("hiring_item_hidden");
        });

        $(".title").click(function () {
            // console.log($('.scroll_nav'))
            $('.scroll_nav').toggleClass("closed");
            $('.scroll__overlay').toggleClass("scroll__overlay__show");
        });
        // end Dropdown hiring



        //Modal
        $(".open_modal").fancybox();
        //End Modal

        //Filter blog
        // $(function() {
        //     var selectedClass = "";
        //     $(".fil_btn").click(function(e){
        //         e.preventDefault();
        //
        //         selectedClass = $(this).attr("data-rel");
        //         $("#post_filter").fadeTo(100, 0.1);
        //         $("#post_filter .tile").not("."+selectedClass).fadeOut().removeClass('scale');
        //         setTimeout(function() {
        //             $("."+selectedClass).fadeIn().addClass('scale');
        //             $("#post_filter").fadeTo(300, 1);
        //         }, 300);
        //
        //     });
        // });
        //End Filter blog


        // if (bowser.firefox) {
        //     DOMs.body.addClass('brow-firefox');
        // }
        // if (bowser.safari) {
        //     DOMs.body.addClass('brow-safari');
        // }
        // if (bowser.msie) {
        //     DOMs.body.addClass('brow-msie');
        // }
        // if (bowser.msedge) {
        //     DOMs.body.addClass('brow-msedge');
        // }
    });

// ========== !!! RESPONSIVE SCRIPTS !!! ===========

    $(window).on('load resize', function () {
        if (window.matchMedia("(max-width: 767px)").matches) {

            // ----- SCROLLING CLASS CHANGE -----
            $(window).scroll(function() {
                // declare variable
                var topPos = $(this).scrollTop();

                // if user scrolls down - add class
                if (topPos > 300) {
                    DOMs.dropdown.addClass("scroll_nav_dropdown_active");
                    DOMs.dropdown_title.addClass("col-sm-12");
                } else {
                    DOMs.dropdown.removeClass("scroll_nav_dropdown_active");
                    DOMs.dropdown_title.removeClass("col-sm-12");
                }
            }); // scroll END


        } else if (window.matchMedia("(min-width: 768px)").matches) {


            //Scroll logo
            if ($(this).scrollTop() > 30) {
                $('#nav').addClass("scroll");
            }

            $(window).scroll(function() {
                if ($(this).scrollTop() > 30) {
                    $('#nav').addClass("scroll");
                } else {
                    $('#nav').removeClass("scroll");
                }
            });
            //End Scroll logo

            /*$(window).scroll(function () {
                if ($(this).scrollTop() > 45) {
                    $('nav').addClass("scroll");
                } else {
                    $('nav').removeClass("scroll");
                }
            });*/
            //End Scroll logo


            DOMs.tabs.rewTabs();

            //Add smooth to btn post filter
            // $('.fil_btn').removeClass('smooth');

            //Svg move mouse
            $(document).on("mousemove", ".link__overlay", function (e) {
                var x = e.clientX - $(this).offset().left - parseFloat($(this).css('paddingLeft').match(/\d+\.?\d*/)[0]) + $(window).scrollLeft();
                var y = e.clientY - $(this).offset().top - parseFloat($(this).css('paddingTop').match(/\d+\.?\d*/)[0]) + $(window).scrollTop();
                $(this).find(".follower").css("transform","translate3d(calc(" + x + "px - 50%),calc(" + y + "px - 50%),0px)");
                // if (not_moved_yet) {
                //     if ($(this).find(".follower").hasClass("follower_out")) {
                //         $(this).find(".follower").removeClass("follower_out");
                //     }
                //     not_moved_yet = 0;
                // }
            });
            $(".link__overlay").on("mouseenter", function (e) {
                var x = e.clientX - $(this).offset().left - parseFloat($(this).css('paddingLeft').match(/\d+\.?\d*/)[0]) + $(window).scrollLeft();
                var y = e.clientY - $(this).offset().top - parseFloat($(this).css('paddingTop').match(/\d+\.?\d*/)[0]) + $(window).scrollTop();
                $(this).find(".follower").css("transform","translate3d(calc(" + x + "px - 50%),calc(" + y + "px - 50%),0px)");
                $(this).find(".follower").removeClass("follower_out");
            });
            $(".link__overlay").on("mouseleave", function (e) {
                $(this).find(".follower").addClass("follower_out");
            });

            //Svg change way
            var scrolledTop = 0, scrolledLeft = 0;
            var mouseX, mouseY;
            var mirrored = 0;
            $(document).on("mousemove", ".box", function (e) {
                var x = e.clientX;
                var y = e.clientY;
                var middle = $(this).width() / 2;
                mouseX = e.pageX;
                mouseY = e.pageY;
                if (x < middle) {
                    if (mirrored) {
                        $("#arrow").removeClass("mirrored");
                        mirrored = 0;
                    }
                } else {
                    if (!mirrored) {
                        $("#arrow").addClass("mirrored");
                        mirrored = 1;
                    }
                }
                $(".circle").css({transform: "translate(calc(" + mouseX + "px - 50%), calc(" + mouseY + "px - 50%)", opacity: "1"});
            });
            $(document).scroll(function (e) {
                if (scrolledLeft != $(document).scrollLeft()) {
                    mouseX -= scrolledLeft;
                    scrolledLeft = $(document).scrollLeft();
                    mouseX += scrolledLeft;
                }
                if (scrolledTop != $(document).scrollTop()) {
                    mouseY -= scrolledTop;
                    scrolledTop = $(document).scrollTop();
                    mouseY += scrolledTop;
                }
                $(".circle").css("transform", "translate(calc(" + mouseX + "px - 50%), calc(" + mouseY + "px - 50%)");
            });
            $(".box").on("mouseenter", function (e) {
                //console.log("enter");
                $(".circle").removeClass("out");
            });
            $(".box").on("mouseleave", function (e) {
                //console.log("leave");
                $(".circle").addClass("out");
            });
            //End Svg change way


            //Scroll sticky nav container
            DOMs.dropdown_sticky.stick_in_parent({
                offset_top: 80
            });
            //End Scroll sticky nav container

        }
    });

})(jQuery);


